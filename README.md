# GitLab Web Debugger
---

###### Disclaimer: This is a side-project of a GitLab employee, *not* an official GitLab product.

### What does it do?

This chrome extension is only active on official GitLab instances. That means https://gitlab.com and other instances used internally at GitLab.  
It aids developers, engineers and customers in identifying the root cause of a page load error.

To do this, it uses the `chrome.webRequest.onCompleted` API to gather the HTTP headers returned by GitLab on the appropriate domains. The main thing it looks for is the correlation ID (`X-Request-ID`). This ID in combination with Sentry can be used by GitLab employees to access the exact error that was triggered on the backend.  
To simplify this process certain GitLab instances are preconfigured in this extension to generate a link, that will directly point to the error in Sentry.

This correlation ID can also help us, when you as a customer provide this ID when reporing an issue. The data behind that ID can only be seen by GitLab employees.

It also prints the load balancer and server hit with the request (where configured server-side).

*This extension cannot access any data not on GitLab domains.*

### How do I install this?

You can grab it from the Chrome web store here: https://chrome.google.com/webstore/detail/gitlab-web-debugger/pjpihpogiidfbgdbmkmmhileaijeakan

That version might lag a few days behind though, so if you need the latet, here is how to install it manually:

1. git clone this repo somewhere
2. access `chrome://extensions/` in Google Chrome
3. turn on `Developer Mode` in the upper right corner
4. click on `Load unpacked` and point the finder to the path where you cloned the repository.
5. done!

### How do I use it?

Once installed, it works for you passively :)  
Should you encounter an error (which hopefully does not happen) or you are just curious, you can click on the GitLab icon in the extension bar to see the details of the last page-load.  
*Note:* This will only work on GitLab domains. If the GitLab icon is greyed out, there is no data being gathered!

### What does it look like?

![boxshot](boxshot.png)

### License?

[Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0)
