if (!window.chrome) {
    window.chrome = window.browser;
}


window.getSentryURL = async (project, corr_id) => {
  if (!project || !corr_id) {
    return null;
  }

  return `https://sentry.gitlab.net/gitlab/${project}/?query=correlation_id%3A%22${corr_id}%22`
};

window.getLogURL = async (corr_ids) => {
  if (!Array.isArray(corr_ids) || corr_ids.length == 0) {
    return null;
  }

  const params = `!("${corr_ids.join('","')}")`
  const value = `${corr_ids.join(',')}`
  const should = `!((match_phrase:(json.correlation_id:'${corr_ids.join('\')),(match_phrase:(json.correlation_id:\'')}')))`

  return `https://log.gprd.gitlab.net/app/kibana#/dashboard/AWfFGg4H5VFy3_25mrdc?_g=()&_a=(description:'To%20investigate%20a%20correlation,%20edit%20the%20%60json.correlation_id.keyword%60%20filter%20below%20the%20search%20bar.',filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:a4f5b470-edde-11ea-81e5-155ba78758d4,key:json.correlation_id,negate:!f,params:${params},type:phrases,value:'${value}'),query:(bool:(minimum_should_match:1,should:${should})))),fullScreenMode:!f,options:(darkTheme:!f),query:(language:lucene,query:''),timeRestore:!f,title:'Correlation%20Dashboard',viewMode:view)`
};

const openTab = (active, url) => {
  return new Promise((resolve, _)=> {
    chrome.tabs.create(
      {
        active: active,
        url: url,
      },
      resolve
    )
  });
}

window.openAll = async (tabId) => {
  console.log('openAll', tabId);

  let corr_ids = [];

  for (let entry of window.requests[tabId]) {
    console.log(entry);
    if (entry && entry.correlation_id) {
      corr_ids.push(entry.correlation_id);
    }
  }

  let tabs = [];

  const kibana = await getLogURL(corr_ids)
  tabs.push(await openTab(true, kibana));

  for (let id of corr_ids) {
    tabs.push(await openTab(false, await getSentryURL(window.requests[tabId][0].sentry_project, id)))
  }

  if (chrome.tabs.group) {
    console.log('has chrome.tabs.group');
      let tab_ids = [];
      for (let tab of tabs) {
        tab_ids.push(tab.id);
      }
      chrome.tabs.group(
        {tabIds: tab_ids},
        (groupId) => {
          console.log(groupId);
        }
      )
  } else {

      console.log('not has chrome.tabs.group');
  }
}

const filter = {
    urls: [
        'https://gitlab.com/*',
        'https://*.gitlab.com/*',
        'https://*.gitlab.net/*',
        'https://*.gitlab.org/*',
    ]
};

const pre_filter = {
    urls: [
        'https://gitlab.com/*',
        'https://*.gitlab.com/*',
        'https://*.gitlab.net/*',
        'https://*.gitlab.org/*',
    ],
    types: ['main_frame']
};

const extraInfoSpec = ['responseHeaders'];

const show = (tab, debug) => {
    if (debug) {
        chrome.pageAction.show(tab);
    } else {
        chrome.pageAction.hide(tab);
    }
}

const getSentryProject = url => {
    if (url.match(/:\/\/ops.gitlab.net\//)) {
        return "opsgitlabnet";
    } else if (url.match(/:\/\/staging.gitlab.com\//)) {
        return "staginggitlabcom";
    } else if (url.match(/:\/\/pre.gitlab.com\//)) {
        return "pregitlabcom";
    } else if (url.match(/:\/\/gitlab.com\//)) {
        return "gitlabcom";
    } else {
        return null;
    }
}

window.requests = {};
window.notificationShown = {};

const getSettings = () => {
  return new Promise((res, _) => {
    chrome.storage.sync.get({
      notifications_enabled: false,
      notifications_nagging: false,
    }, res);
  });
}

const processCompletedRequest = async (details) => {

    if ((details.statusCode == 304 || details.statusCode == 200) && details.type !== "main_frame") {
        // Discard all OK traffic. This would just blow up memory usage.
        return;
    }

    const headers = details.responseHeaders.reduce((accumulator, current) => {
        return Object.defineProperty(accumulator || {}, current.name.toLowerCase(), {
            configurable: true,
            value: current.value,
            writable: false
        });
    })
    const out = {};

    const sentry_project = getSentryProject(details.url);
    if (!sentry_project && details.type !== "main_frame") {
        // We do not want any subsequent requests to a non-GitLab product.
        // But we want the main_frame, so we can display the UI without error.
        return;
    }

    const notifications_enabled = !!(await getSettings()).notifications_enabled;
    const notifications_nagging = !!(await getSettings()).notifications_nagging;

    out['sentry_project'] = sentry_project;
    out['correlation_id'] = headers['x-request-id'] || null;
    out['cf_ray_id'] = headers['cf-ray'] || null;
    out['loadbalancer'] = headers['gitlab-lb'] || null;
    out['server'] = headers['gitlab-sv'] || null;
    out['method'] = details.method;
    out['url'] = details.url;
    out['type'] = details.type;
    out['statusCode'] = details.statusCode;
    out['timeStamp'] = details.timeStamp;

    if (!window.requests[details.tabId]) {
        beforeMainFrame(details);
    }
    if (details.type !== "main_frame") {
      window.notificationShown[details.tabId] = false;
    }
    window.requests[details.tabId].push(out);

    if (notifications_enabled && details.statusCode >= 400 && !window.notificationShown[details.tabId]) {
        window.notificationShown[details.tabId] = true;
        chrome.notifications.create(
            `${details.tabId}`,
            {
                type: 'basic',
                iconUrl: '../../icons/icon128.png',
                eventTime: details.timeStamp,
                title: 'GLWD failed request',
                contextMessage: window.requests[details.tabId][0].url,
                message: 'There was at least 1 failed request.',
                priority: 2,
                requireInteraction: notifications_nagging,
                silent: !notifications_nagging,
            });
    }
};

const beforeMainFrame = (details) => {
    window.requests[details.tabId] = [];
}

chrome.webRequest.onBeforeRequest.addListener(beforeMainFrame, pre_filter);
chrome.webRequest.onCompleted.addListener(processCompletedRequest, filter, extraInfoSpec);
chrome.notifications.onClicked.addListener(
async (notificationId) => {
  try {
    window.notificationShown[parseInt(notificationId)] = false
    chrome.tabs.get(parseInt(notificationId), (tab) => {
        chrome.tabs.highlight({'tabs': tab.index});
        show(tab.id, window.requests[tab.id]);
    });
  } catch (_) {}
},
);

chrome.tabs.onActivated.addListener((activeInfo) => {
    if (!window.requests[activeInfo.tabId]) {
        return
    }
    show(activeInfo.tabId, window.requests[activeInfo.tabId]);
});

chrome.tabs.onUpdated.addListener((tabId) => {
    if (!window.requests[tabId]) {
        return
    }
    show(tabId, window.requests[tabId]);
});

chrome.tabs.onRemoved.addListener((tabId) => {
    delete window.requests[tabId];
    delete window.notificationShown[tabId]
    chrome.notifications.clear(`${tabId}`,);
});
